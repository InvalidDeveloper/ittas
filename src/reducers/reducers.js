import {fieldsEn, fieldsRus} from '../languages'

const reducer = (state, action) => {

    if (state === undefined) {
        return {
            language: 'rus',
            fields: fieldsRus,
            legalFormDTOList: [],
        }
    }

    switch (action.type) {
        case 'ENGLISH_LANGUAGE':
            return {
                ...state,
                language: 'en',
                fields: fieldsEn,
            }
        case 'RUSSIAN_LANGUAGE':
            return {
                ...state,
                language: 'rus',
                fields: fieldsRus,
            }
        case 'DATA_LOADED': {
            return {
                ...state,
                legalFormDTOList: action.payload
            }
        }
        default:
            return state
    }
}

export default reducer