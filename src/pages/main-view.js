import React, {Fragment} from 'react'
import {connect} from 'react-redux'

import RegistrationForms from '../components/registration-forms/registration-forms'


const MainView = ({title}) => {
    return (
        <Fragment>
            <h1 className='col-12'>{title}</h1>
            <RegistrationForms />
        </Fragment>
    )
}

const mapDispatchToProps = ({fields: {mainPage: {title}}}) => {
    return {title}
}

export default connect(mapDispatchToProps)(MainView)