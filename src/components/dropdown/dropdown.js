import React, {Component} from 'react'
import Select from 'react-select'

import style from './dropdown.module.scss'


class DropDown extends Component {
    state = {
        selectedOption: null,
    }
    handleChange = (selectedOption) => {
        this.setState({selectedOption})
        console.log(`Option selected:`, selectedOption)
    }

    render() {

        const {options} = this.props
        const {selectedOption} = this.state

        return (
            <Select
                className={style.dropDown}
                value={selectedOption}
                onChange={this.handleChange}
                options={options}
                isClearable={true}
                isSearchable={true}
            />
        )
    }
}

export default DropDown