import React from 'react'
import {Link} from 'react-router-dom'
import {connect} from 'react-redux'
import classNames from 'classnames'

import {englishLanguage, russianLanguage} from '../../actoin-creators/language-creator'

import style from './header.module.scss'


const Header = ({language, title, english, russian}) => {

    const headerClass = classNames(style.header)
    const logoClass = classNames(style.logo, 'text-dark')

    return (
        <header role='banner' className={headerClass}>
            <Link to='/'>
                <div className={logoClass}>{title}</div>
            </Link>
            <div className={style.language}>
                <button onClick={russian}>Русский</button>
                <button onClick={english}>English</button>
            </div>
        </header>
    )
}

const mapStateToProps = ({language, fields: {title}}) => {
    return {language, title}
}

const mapDispatchToProps = {
    english: englishLanguage,
    russian: russianLanguage,
}

export default connect(mapStateToProps, mapDispatchToProps)(Header)