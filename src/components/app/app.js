import React from 'react'
import {Switch, Route} from 'react-router-dom'

import {MainPage} from '../../pages'
import Header from '../header'

const App = () => {
    return (
        <div>
            <Header/>
            <Switch>
                <Route path='/'
                       render={MainPage}
                       exact/>
                <Route render={() => <h2>Page not found</h2>}/>
            </Switch>
        </div>
    )
}

export default App