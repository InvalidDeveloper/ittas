import React, {Component} from 'react'
import {connect} from 'react-redux'
import classNames from 'classnames'
import {bindActionCreators} from 'redux'

import DropDown from '../dropdown'
import {booksLoaded} from '../../actoin-creators/dictionaries-creator'
import {dictionaryLoading} from '../../services/rest-service'

import style from './registration-forms.module.scss'

class RegistrationForms extends Component {

    componentDidMount() {
        dictionaryLoading.then(({legalFormDTOList}) => {
            return this.props.booksLoaded(legalFormDTOList)
        })
    }

    render() {
        const {
            legalFormDTOList = [],
            mainPage: {
                placeholderOrganization,
                placeholderName,
                placeholderInn,
                placeholderShortName,
                placeholderLegalAddress,
                placeholderActualAddress,
                placeholderTelephoneNumber,
                placeholderEmail,
                placeholderAdmTerr,
                placeholderTax,
                placeholderTaxSystem,
            },
        } = this.props
        const clazzForm1 = classNames(style.forms, 'container', 'col-8')
        const clazzForm2 = classNames(style.forms, 'container', 'col-4')
        const mapper = legalFormDTOList.map((item) => {
            return {...item, label: item && item.name, value: item && item.name}
        })
        return (
            <div className={style.main}>
                <div className={clazzForm1}>
                    <div className='row'>
                        <DropDown options={mapper}/>
                        <input
                            placeholder={placeholderName}/>
                    </div>
                    <div className='row'>
                        <input
                            placeholder={placeholderInn}/>
                        <input
                            placeholder={placeholderShortName}/>
                    </div>
                    <div className='row'>
                        <input
                            placeholder={placeholderLegalAddress}/>
                        <input
                            placeholder={placeholderActualAddress}/>
                    </div>
                    <div className='row'>
                        <input
                            placeholder={placeholderTelephoneNumber}/>
                        <input
                            placeholder={placeholderEmail}/>
                    </div>
                </div>
                <div className={clazzForm2}>
                    <div className='row'>
                        <input
                            placeholder={placeholderAdmTerr}/>
                    </div>
                    <div className='row'>
                        <input
                            placeholder={placeholderTax}/>
                    </div>
                    <div className='row'>
                        <input
                            placeholder={placeholderTaxSystem}/>
                    </div>
                </div>
            </div>


        )
    }

}

const mapStateToProps = ({legalFormDTOList, fields: {mainPage}}) => {
    return {
        legalFormDTOList,
        mainPage,
    }
}

const mapDispatchToProps = {
    booksLoaded
}

export default connect(mapStateToProps,mapDispatchToProps)(RegistrationForms)