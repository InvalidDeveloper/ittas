import fieldsRus from './russian-language'
import fieldsEn from './english-language'

export {
    fieldsEn,
    fieldsRus
}