const fieldsEn = {
    title: 'Registration',
    mainPage: {
        title: 'Application for registration of CX in the system',
        placeholderOrganization: 'Organizational and Legal Form',
        placeholderName: 'Name',
        placeholderInn: 'Inn',
        placeholderShortName: 'Short name',
        placeholderLegalAddress: 'Legal address',
        placeholderActualAddress: 'Actual address',
        placeholderTelephoneNumber: 'Phone',
        placeholderEmail: 'e-mail',
        placeholderAdmTerr: 'Administrative territorial unit',
        placeholderTax: 'Tax authority',
        placeholderTaxSystem: 'Taxation system',
    }
}

export default fieldsEn