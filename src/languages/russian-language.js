const fieldsRus = {
    title: 'Регистрация',
    mainPage: {
        title: 'Заявка на регистрацию СХ в системе',
        placeholderOrganization: 'Организационно-Правовая форма',
        placeholderName: 'Наименование',
        placeholderInn: 'Инн',
        placeholderShortName: 'Сокращенное наименование',
        placeholderLegalAddress: 'Юридический адресс',
        placeholderActualAddress: 'Фактический адресс',
        placeholderTelephoneNumber: 'Телефон',
        placeholderEmail: 'e-mail',
        placeholderAdmTerr: 'Администротивно-территор-я ед.',
        placeholderTax: 'Налоговый орган',
        placeholderTaxSystem: 'Система налогооблажения',
    }
}

export default fieldsRus