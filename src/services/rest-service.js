// export const methodsEnum = {
//     get: 'GET',
//     post: 'POST',
//     put: 'PUT',
//     delete: 'DELETE',
// }
//
//
// export const fetchData = ({method, data, url}) => {
//     const init = {
//         method,
//         headers: {
//             'Content-Type': 'application/json',
//         },
//         mode: 'cors',
//         cache: 'default',
//     }
//
//     return fetch(`http://192.168.105.114:8080/company/v2/api-docs/${url}`, init)
//         .then((response) => {
//             return response.json()
//         })
//         .catch(console.error)
// }
//
//
// export const organizationFormDic = fetchData({
//     method: methodsEnum.get,
//     url: 'rest/internal/dictionary/legal-form/all'
// })

export const dictionaryLoading = fetch('http://192.168.105.114:8080/company/rest/internal/dictionary/legal-form/all').then(data => {
    return data.json()
})
    .catch((err) => {
        console.log(err)
    })