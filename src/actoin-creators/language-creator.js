const englishLanguage = () => {
    return {
        type: 'ENGLISH_LANGUAGE',
    }
}

const russianLanguage = () => {
    return {
        type: 'RUSSIAN_LANGUAGE',
    }
}

export {
    englishLanguage,
    russianLanguage,
}