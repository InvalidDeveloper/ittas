const booksLoaded = (newBooks) => {
    return {
        type: 'DATA_LOADED',
        payload: newBooks,
    }
}
export {
    booksLoaded,
}